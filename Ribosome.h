#pragma once
#include "AminoAcid.h"
#include "Protein.h"
#include "Nucleus.h"

class Ribosome {
	Protein* create_protein(std::string &RNA_transcript) const;
};