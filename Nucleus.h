#pragma once
#include <iostream>

class Gene {
	private:
		unsigned int _start;
		unsigned int _end;
		bool strand_dna_complementary_on;
		void set_on_complementary_dna_strand(bool const strand);
		void set_start(unsigned int const start);
		void set_end(unsigned int const end);
	public:
		bool is_on_complementary_dna_strand();
		unsigned int get_start()const;
		unsigned int  get_end()const;
		void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
};

class Nucleus {
	private:
		std::string _DNA_strand;
		std::string _complementary_DNA_strand;
		std::string get_RNA_transcript(const Gene& gene) const;
		std::string get_reversed_DNA_strand() const;
		unsigned int get_num_of_codon_appearances(const std::string& codon) const;
		void set_DNA_strand(std::string _DNA_strand);
		void set_complementary_DNA_strand(std::string _complementary_DNA_strand);
	public:
		void init(const std::string dna_sequence);
		std::string get_DNA_strand();
		std::string get_complementary_DNA_strand();
};