/*********************************
* Class: MAGSHIMIM C2			 *
* Week:                			 *
* Name:                          *
* Credits:                       *
**********************************/

#include <string>
#include <stdio.h>
#include <iostream>
#include "Cell.h"
#include "Nucleus.h"

void Gene::set_on_complementary_dna_strand(bool const strand)
{
	this->strand_dna_complementary_on = strand;
}


void Gene::set_start(unsigned int const start)
{
	if(start>0)
		this->_start = start;
}



void Gene::set_end(unsigned int const end)
{
	if(end>this->get_start)
		this->_end = end;
}



void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->set_on_complementary_dna_strand(start);
	this->set_start(end);
	this->set_end(on_complementary_dna_strand);
}

unsigned int Gene::get_start()const
{
	return Gene::_start;
}

bool Gene::is_on_complementary_dna_strand()
{
	return this->strand_dna_complementary_on;
}



unsigned int  Gene::get_end()const
{
	return this->_end;
}



std::string Nucleus::get_DNA_strand()
{
	return this->_DNA_strand;
}


std::string Nucleus::get_complementary_DNA_strand()
{
	return this->_complementary_DNA_strand;
}


std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	int i = 0, index = 0;
	std::string rna = new char[this->_DNA_strand.length()];
	std::string final_rna = new char[gene.get_end - gene.get_start +1];
	if (gene.is_on_complementary_dna_strand)
	{
		rna = this->_complementary_DNA_strand;
	}
	else
	{
		rna = this->_DNA_strand;
	}
	final_rna = rna.substr(gene.get_start,gene.get_end);
	for (i = 0; i < final_rna.length(); i++)
	{
		if (final_rna[i] == 'T')
		{
			final_rna[i] = 'U';
		}
	}
	return final_rna;
}



std::string Nucleus::get_reversed_DNA_strand() const
{
	int k = 0, l = 0;
	std::string rev = new char[_DNA_strand.length()];
	for (l = 0; l < rev.length(); l++)
	{
		rev[l] = 0;
	}
	for (int i = _DNA_strand.length(); i >= 0; i--)
	{
		rev[k] = _DNA_strand[i];
		k++;
	}
	return rev;
}



unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	int count = 0;
	if (this->_DNA_strand.find(codon)!= std::string::npos)
	{
		count++;
		this->_DNA_strand.find(codon);
	}
	return count;
}



void Nucleus::set_DNA_strand(std::string _DNA_strand)
{
	this->_DNA_strand = _DNA_strand;
}



void Nucleus::set_complementary_DNA_strand(std::string _complementary_DNA_strand)
{
	this->_complementary_DNA_strand = _complementary_DNA_strand;
}



void Nucleus::init(const std::string dna_sequence)
{
	int i = 0,index=0;
	std::string complementary_DNA_strand = new char[dna_sequence.length()];
	set_DNA_strand(dna_sequence);
	for (i = 0; i < dna_sequence.length(); i++)
	{
		if (dna_sequence[i] != 'A' && dna_sequence[i] != 'C' && dna_sequence[i] != 'G' && dna_sequence[i] != 'T')
		{
			std::cerr << "invalid dna string";
		}


		if (dna_sequence[i] == 'A')
		{
			complementary_DNA_strand[i] = 'T';
		}


		else if (dna_sequence[i] == 'G')
		{
			complementary_DNA_strand[i] = 'C';
		}


		else if (dna_sequence[i] == 'C')
		{
			complementary_DNA_strand[i] = 'G';
		}


		else if (dna_sequence[i] == 'T')
		{
			complementary_DNA_strand[i] = 'A';
		}
	}
	this->set_complementary_DNA_strand(complementary_DNA_strand);
}
