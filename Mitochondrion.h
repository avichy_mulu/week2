#pragma once
#include "Protein.h"
#include "AminoAcid.h"

class Mitochondrion {
	unsigned int _level_glocuse;
	bool _receptor_glocuse_has;
public:
	void init();
	void insert_glucose_receptor(const Protein & protein);
	void set_glucose(const unsigned int glocuse_units);
	unsigned int get_level_glocuse()const;
	bool does_receptor_glocuse_has()const;
	bool produceATP() const;
};