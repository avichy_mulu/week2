#include "Ribosome.h"

Protein* Ribosome::create_protein(std::string &RNA_transcript) const
{
	int i = 0;
	Protein* protein = new Protein();
	protein->init();
	std::string curr_trio =new char[3];
	AminoAcid result= UNKNOWN;
	for (i = 0; i < RNA_transcript.length(); i++)
	{
		if (RNA_transcript.length() - i > 3)
		{
			curr_trio = RNA_transcript.substr(i, i+2);
			result = get_amino_acid(curr_trio);
			if (result == UNKNOWN)
			{
				return nullptr;
			}
			else
			{
				protein->add(result);
			}
		}
	}
}